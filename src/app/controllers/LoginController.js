const jwt = require('jsonwebtoken');
const authPassword = require('../middlewares/authPassword');
const User = require('../models/User');

module.exports = {
  async login(req, res) {
    const user = await User.findOne({
      where: { email: req.body.email },
    });

    if (!user) {
      res.status(404).send('Usuário não encontrado.');
    }

    if (authPassword.check(req.body.password, user.password_hash)) {
      res.json({
        user,
        token = jwt.sign(
          user,
          process.env.SECRET, { expiresIn: '10m' }),
      });
    } else {
      res.send('Erro de autenticação, verifique seus dados');
    }
  },
};
