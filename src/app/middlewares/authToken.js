const jwt = require('jsonwebtoken');
require('dotenv/config');

const authToken = function authenticateToken(req, res, next) {
  const authHeader = req.headers.authorization;

  if(!authHeader){
    return res.sendStatus(401);
  }

  const token = authHeader.split(' ')[1];

  if (token == null) {
    return res.status(401).send('Sem token');
  }

  jwt.verify(token, process.env.SECRET, (err, user) => {
    if (err) return res.status(403).send('Token iválido');
    req.user = user;
  });

  return next();
};

module.exports = authToken;
