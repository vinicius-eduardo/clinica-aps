const express = require('express');
const cors = require('cors');
const routes = require('./routes');
require('dotenv/config');
require('./database');

const app = express();
app.use(cors());
app.use(express.json());

app.use(routes);

const PORT = process.env.SERVER_PORT || 3333;

app.listen(PORT, () => {
  global.console.log(`Server Running at ${PORT}`);
});
